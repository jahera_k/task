<?php 

  include('db_con.php');

  session_start();
  if(isset($_POST['records-limit'])){
      $_SESSION['records-limit'] = $_POST['records-limit'];
  }
  
  $limit = isset($_SESSION['records-limit']) ? $_SESSION['records-limit'] : 2;
  $page = (isset($_GET['page']) && is_numeric($_GET['page']) ) ? $_GET['page'] : 1;
  
  $paginationStart = ($page - 1) * $limit;
  $totalEmpSQL = "SELECT * FROM details";
  $allEmpResult = mysqli_query($conn, $totalEmpSQL);
  $totalEmployee = mysqli_num_rows($allEmpResult);
  $toalPages  = ceil($totalEmployee/$limit);
  $empSQL = "SELECT * FROM `details` LIMIT $paginationStart, $limit";
  $empResult = mysqli_query($conn, $empSQL);

  $prev = $page - 1;
  $next = $page + 1;
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <style>
        .container {
            max-width: 1000px
        }

    
    </style>
</head>

<body>
  
        <div class="container">
			<br><br>
            <form action="data.php" method="post">
                <select name="records-limit" id="records-limit" >
                    <option disabled selected>Records Limit</option>
                    <?php foreach([2,3,4,5,7,10,12] as $limit) : ?>
                    <option
                        <?php if(isset($_SESSION['records-limit']) && $_SESSION['records-limit'] == $limit) echo 'selected'; ?>
                        value="<?= $limit; ?>">
                        <?= $limit; ?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </form>
        <br><br>

        <table border="2" >
	
		<tr> 
			<td>ID</td> 
			<td>Name</td> 
			<td>password</td>
            
		</tr>
	<tbody> 
		<?php 
		while($emp = mysqli_fetch_assoc($empResult)){
		?>
			<tr> 
				<td><?php echo $emp['id']; ?></td> 
				<td><?php echo $emp['name']; ?></td> 
				<td><?php echo $emp['password']; ?></td> 
               

			</tr> 
		<?php } ?>
	</tbody> 
	</table>
	<br><br>
	<?php
	if($page>1)
	{
	echo "<a  href='data.php?page=".$prev."'> Previous </a>";
	}

	for($i=1;$i<=$toalPages;$i++)
	{
		
		echo "<a  href='data.php?page=".$i."'>".$i. "</a>";

	}
	if($page<$toalPages)
	{
	echo "<a  href='data.php?page=".$next."'> Next></a>";
	}

	?>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#records-limit').change(function () {
                $('form').submit();
            })
        });
    </script>
</body>

</html>